import unittest
from mock import MagicMock
import time
import uuid

from datetime import datetime, tzinfo, timedelta
from decimal import Decimal
from collections import Mapping, Sequence, Set

from proroute_utilities import ProRouteUtilities
from proroute_utilities import SimpleUTC
from proroute_utilities import DecimalEncoder


# python 2 /3 compatibility
try:
    basestring
except NameError:
    basestring = str

try:
    long
except NameError:
    long = int
    

def fun(x):
    return x + 1

class ProRouteUtilitiesTest(unittest.TestCase):

    def test_simple_example(self):
        self.assertEqual(fun(3), 4)

    def test_load_input_data(self):
        mocked_event = {
            "body": "body content"
        }
        ProRouteUtilities.load_input_data(mocked_event)

    def test_load_input_data_failure(self):
        mockEvent = MagicMock(side_effect=Exception)
        self.assertRaises(Exception, mockEvent)

    def test_load_query_data(self):
        mocked_event = {
            "query": "query content"
        }
        ProRouteUtilities.load_query_data(mocked_event)

    def test_load_query_data_failure(self):
        mockEvent = MagicMock(side_effect=Exception)

        self.assertRaises(Exception, mockEvent)

    def test_load_request_data(self):
        mocked_event = {
            "request": "body content"
        }
        ProRouteUtilities.load_request_data(mocked_event)

    def test_load_request_data_failure(self):
        mockEvent = MagicMock(side_effect=Exception)
        self.assertRaises(Exception, mockEvent)

    def test_validate_input_data_success(self):
        mock_input_data = ['name', 'id', 'location', 'email']
        mock_required_data_attr = ['id', 'email']

        ProRouteUtilities.validate_input_data(mock_input_data, mock_required_data_attr)

    def test_validate_input_data_failure(self):
        # Missing required data from input
        mockData = MagicMock(side_effect=Exception)
        self.assertRaises(Exception, ProRouteUtilities.validate_input_data(mockData,mockData))

    def test_populate_new_item_from_data_success(self):
        mock_required_data_attr = ['id', 'email']
        mock_input_data = {
            'email': 'dummy email',
            'id': 'dummy id'
        }

        mock_item = ProRouteUtilities.populate_new_item_from_data(mock_input_data, mock_required_data_attr)

        self.assertIn('email', mock_item)
        self.assertIn('createdAt', mock_item)
        self.assertIn('createdDate', mock_item)
        self.assertIn('updatedDate', mock_item)
        self.assertIn('updatedAt', mock_item)
        self.assertIn('id', mock_item)

    def test_populate_existing_item_from_data(self):
        mock_required_data_attr = ['id', 'email']
        mock_input_data = {
            'email': 'dummy email',
            'id': 'dummy id'
        }

        new_item = {
            'email': 'new_email'
        }

        mock_item = ProRouteUtilities.populate_new_item_from_data(mock_input_data, mock_required_data_attr)

        updated_mock_item = ProRouteUtilities.populate_existing_item_from_data(new_item, mock_item, mock_required_data_attr)

        self.assertIn('email', updated_mock_item)
        self.assertIn('createdAt', updated_mock_item)
        self.assertIn('createdDate', updated_mock_item)
        self.assertIn('updatedDate', updated_mock_item)
        self.assertIn('updatedAt', updated_mock_item)
        self.assertIn('id', updated_mock_item)
        self.assertEquals(new_item['email'], updated_mock_item['email'])

    def test_dynamodb_sanitize(self):

        current_timestamp = int(time.time() * 1000)
        current_utc_datetime_iso8601 = ProRouteUtilities.get_utc_iso8601_datetime()

        mock_required_data_attr = ['id', 'email']
        mock_input_data = {
            'email': 'dummy email',
            'bool': 'false',
            'int_str': '90',
            'int': 90,
            'id': str(uuid.uuid1()),
            'createdAt': current_timestamp,
            'createdDate': current_utc_datetime_iso8601,
            'updatedAt': current_timestamp,
            'updatedDate': current_utc_datetime_iso8601
        }

        mock_item = ProRouteUtilities.populate_new_item_from_data(mock_input_data, mock_required_data_attr)

        ProRouteUtilities.dynamodb_sanitize(mock_item)

    def test_get_dynamo_db_table(self):
        pass

    def test_get_dynamo_alt_env_db_table(self):
        pass

    def test_get_dynamo_alt_db_table(self):
        ProRouteUtilities.get_dynamo_alt_db_table('table-name')

    def test_populate_scan_filter(self):
        mock_scan_options = {
            "items": "items"
        }
        mock_query_data = {
            "str": "query content",
            "bool_str": "True",
            "bool": True,
            "int": 90,
            "int_str": "90"
        }

        ProRouteUtilities.populate_scan_filter(mock_query_data,mock_scan_options)

    def test_get_utc_iso8601_datetime(self):
        time = datetime.utcnow().replace(tzinfo=SimpleUTC()).isoformat()
        # Checking the first 20 integers to avoid small delays
        self.assertEquals(ProRouteUtilities.get_utc_iso8601_datetime()[0:20], time[0:20])

    def test_str2bool(self):
        bool_str = ["yes", "y", "true", "t", "1"]
        for bool in bool_str:
            self.assertEquals(ProRouteUtilities.str2bool(bool), True)

    def test_str2bool_fails(self):
        self.assertEquals(ProRouteUtilities.str2bool("0"), False)

    # Decimal encoder
    def test_default(self):
        pass








