# ProRoute Utilities Library

A collection of utility functions for ProRoute lambdas

## Updating
  - Run `python setup.py install`. You may see `error: [Errno 13] Permission denied: '/Library/Python/2.7/site-packages/proroute_utilities.py'`, you can ignore this.
  - Add, commit and push the changes.
  - Update in lambdas by running `pip install -r requirements.txt -t vendored --upgrade --no-cache-dir` then redeploy.

## Using
  - Add `https://bitbucket.org/proroute/proroute-utilities/get/master.tar.gz` to requirements.txt
  - Run `pip install -r requirements.txt -t vendored --upgrade --no-cache-dir`
  - Import using `from proroute_utilities import ProRouteUtilities`
  - Deploy