"""A collection of utility functions for ProRoute lambdas"""
import os, sys
import json
import logging
import time
import uuid

from datetime import datetime, tzinfo, timedelta
from decimal import Decimal
from collections import Mapping, Sequence, Set

import boto3
DYNAMO_DB = boto3.resource('dynamodb')

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.DEBUG)

# python 2 /3 compatibility
try:
    basestring
except NameError:
    basestring = str

try:
    long
except NameError:
    long = int

class ProRouteUtilities(object):


    """An :class:`ProRouteUtilities <proroute_utilities.proroute_utilities.ProRouteUtilities>` object."""

    @classmethod
    def load_input_data(cls, event):
        """Return the lambda event body"""
        try:
            input_data = event['body']

        except Exception as e:
            raise Exception('[500] Internal server error: {} '.format(str(e)))

        return input_data

    @classmethod
    def load_cognito_claim(cls, event):
        """Return the lambda event body"""
        try:
            input_data = event['cognitoPoolClaims']

        except Exception as e:
            raise Exception('[500] Internal server error: {} '.format(str(e)))

        return input_data

    @classmethod
    def load_query_data(cls, event):
        """Return the lambda event query"""
        try:
            query_data = event['query']

        except Exception as e:
            raise Exception('[500] Internal server error: {} '.format(str(e)))

        return query_data

    @classmethod
    def load_request_data(cls, event):
        """Return the lambda event request"""
        try:
            request_data = event['request']

        except Exception as e:
            raise Exception('[500] Internal server error: {} '.format(str(e)))

        return request_data

    @classmethod
    def validate_input_data(cls, input_data, required_data_attrs):
        """Checks `input_data` has all attributes in required_data_attrs`."""
        missing_attrs = []
        if required_data_attrs:
            for attr in required_data_attrs:
                if attr not in input_data:
                    logging.error("Validation Failed, missing {}".format(attr))
                    missing_attrs.append(attr)

            if len(missing_attrs) > 0:
                missing_attrs_str = ",".join(missing_attrs)
                raise Exception("[400] Data Validation Failed, missing: {}".format(missing_attrs_str))

        return input_data

    @classmethod
    def default_values(cls, item, default_attrs):
        """Adds values in `default_attrs` to `item`."""
        if default_attrs:
            for key, default_value in default_attrs.items():
                if key not in item:
                    item[key] = default_value

        return item

    @classmethod
    def populate_new_item_from_data(cls, input_data, attrs):
        """Create an item ready for insertion into the databse"""
        current_timestamp = int(time.time() * 1000)
        current_utc_datetime_iso8601 = cls.get_utc_iso8601_datetime()

        # Construct data
        item = {
            'id': str(uuid.uuid1()),
            'createdAt': current_timestamp,
            'createdDate': current_utc_datetime_iso8601,
            'updatedAt': current_timestamp,
            'updatedDate': current_utc_datetime_iso8601
        }

        # Automap data
        for attr in attrs:
            if attr in input_data:
                item[attr] = input_data[attr]

        return item

    @classmethod
    def populate_existing_item_from_data(cls, input_data, item, attrs):
        """Update existing attributes and add new attributes to existing item"""
        current_timestamp = int(time.time() * 1000)
        current_utc_datetime_iso8601 = cls.get_utc_iso8601_datetime()

        # Automap data
        for attr in attrs:
            if attr in input_data:
                item[attr] = input_data[attr]

        if 'updatedAt' in item:
            item['updatedAt'] = current_timestamp
        if 'updatedDate' in item:
            item['updatedDate'] = current_utc_datetime_iso8601

        return item

    @classmethod
    def dynamodb_sanitize(cls, data):
        """Sanitizes an object so it can be updated to dynamodb (recursive). """
        if not data and isinstance(data, (basestring, Set)):
            new_data = None  # empty strings/sets are forbidden by dynamodb
        elif isinstance(data, (basestring, bool)):
            new_data = data  # important to handle these one before sequence and int!
        elif isinstance(data, Mapping):
            new_data = {key: cls.dynamodb_sanitize(data[key]) for key in data}
        elif isinstance(data, Sequence):
            new_data = [cls.dynamodb_sanitize(item) for item in data]
        elif isinstance(data, Set):
            new_data = {cls.dynamodb_sanitize(item) for item in data}
        elif isinstance(data, (float, int, long, complex)):
            new_data = Decimal(str(data))
        elif isinstance(data, (datetime)):
            new_data = data.replace(tzinfo=SimpleUTC()).isoformat()
        else:
            new_data = data

        return new_data

    @classmethod
    def get_dynamo_db_table(cls):
        """Return a dynamodb.Table object of the table referenced by the 'DYNAMODB_TABLE' environment property."""
        return DYNAMO_DB.Table(cls.get_environ_prop('DYNAMODB_TABLE'))

    @classmethod
    def get_dynamo_alt_env_db_table(cls, env_table_name):
        """Return a dynamodb.Table object of the table referenced by the provided environment property."""
        return DYNAMO_DB.Table(cls.get_environ_prop(env_table_name))

    @classmethod
    def get_dynamo_alt_db_table(cls, table_name):
        """Return a dynamodb.Table object of the provided table name."""
        return DYNAMO_DB.Table(table_name)

    @classmethod
    def populate_scan_filter(cls, query_data, scan_options_attrs):
        """Build the filters for a boto dynamodb query. Return the filter object"""
        scan_attrs = {}
        if scan_options_attrs:
            for attr, attr_type in scan_options_attrs.items():
                if attr in query_data:
                    logging.error("scan, data {}".format(attr))
                    if attr_type == "int" or attr_type == "number" or attr_type == "float" or attr_type == "decimal":
                        scan_attrs[attr] = Decimal(query_data[attr])
                    elif attr_type == "boolean":
                        scan_attrs[attr] = cls.str2bool(query_data[attr])
                    else:
                        scan_attrs[attr] = query_data[attr]

        if scan_attrs:
            filter_expression = ''
            expression_attribute_values = {}
            expression_attribute_names = {}
            for key, scan_value in scan_attrs.items():
                if filter_expression != '':
                    filter_expression += ' AND '
                filter_expression += ' #{} = :{}'.format(str(key),str(key))
                expression_attribute_values[':{0}'.format(str(key))]=cls.dynamodb_sanitize(scan_value)
                expression_attribute_names['#{0}'.format(str(key))]=cls.dynamodb_sanitize(key)

            operation_parameters = {
                'FilterExpression': filter_expression,
                'ExpressionAttributeValues': expression_attribute_values,
                'ExpressionAttributeNames': expression_attribute_names
            }
        else:
            operation_parameters = {}

        return operation_parameters

    @classmethod
    def populate_scan_filter_ops(cls, query_data, scan_options_attrs, scan_options_ops):
        """Build the filters for a boto dynamodb query. Return the filter object"""
        scan_attrs = {}
        if scan_options_attrs:
            for attr, attr_type in scan_options_attrs.items():
                if attr in query_data:
                    logging.error("scan, data {}".format(attr))
                    if attr_type == "int" or attr_type == "number" or attr_type == "float" or attr_type == "decimal":
                        scan_attrs[attr] = Decimal(query_data[attr])
                    elif attr_type == "boolean":
                        scan_attrs[attr] = cls.str2bool(query_data[attr])
                    else:
                        scan_attrs[attr] = query_data[attr]

        if scan_attrs:
            filter_expression = ''
            expression_attribute_values = {}
            expression_attribute_names = {}
            for key, scan_value in scan_attrs.items():
                if filter_expression != '':
                    filter_expression += ' AND '
                if scan_options_ops and key in scan_options_ops:
                    if scan_options_ops[key] == 'equal':
                        filter_expression += ' #{} = :{}'.format(str(key),str(key))
                    elif scan_options_ops[key] == 'lesser':
                        filter_expression += ' #{} < :{}'.format(str(key),str(key))
                    elif scan_options_ops[key] == 'greater':
                        filter_expression += ' #{} > :{}'.format(str(key),str(key))
                    elif scan_options_ops[key] == 'lesser_equal':
                        filter_expression += ' #{} <= :{}'.format(str(key),str(key))
                    elif scan_options_ops[key] == 'greater_equal':
                        filter_expression += ' #{} >= :{}'.format(str(key),str(key))
                    elif scan_options_ops[key] == 'begin':
                        filter_expression += ' begins_with(#{}, :{})'.format(str(key),str(key))
                    else:
                        filter_expression += ' #{} = :{}'.format(str(key),str(key))
                else:
                    filter_expression += ' #{} = :{}'.format(str(key),str(key))
                expression_attribute_values[':{0}'.format(str(key))]=cls.dynamodb_sanitize(scan_value)
                expression_attribute_names['#{0}'.format(str(key))]=cls.dynamodb_sanitize(key)

            operation_parameters = {
                'FilterExpression': filter_expression,
                'ExpressionAttributeValues': expression_attribute_values,
                'ExpressionAttributeNames': expression_attribute_names
            }
        else:
            operation_parameters = {}

        return operation_parameters

    @classmethod
    def get_utc_iso8601_datetime(cls):
        """Return a utc iso8601 datetime"""
        return datetime.utcnow().replace(tzinfo=SimpleUTC()).isoformat()

    @classmethod
    def str2bool(cls, bool):
        """Return a boolean of the provided boolean string"""
        return bool.lower() in ("yes", "y", "true", "t", "1")
    
    @classmethod
    def get_environ_prop(cls,property):
        return os.environ[property]

    @classmethod
    def get_boto3_client(cls,client):
        return boto3.client(client)

    @classmethod
    def get_boto3_resource(cls,resource):
        return boto3.resource(resource)

class SimpleUTC(tzinfo):
    """Simple class for timezone management"""
    def tzname(self):
        return "UTC"
    def utcoffset(self, dt):
        return timedelta(0)

class DecimalEncoder(json.JSONEncoder):
    """JSON encoding for Decimal objects"""
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return super(DecimalEncoder, self).default(obj)

