from distutils.core import setup

setup(name='ProRoute Utilities',
      version='1.1',
      description='A collection of utility functions for ProRoute lambdas',
      author='Darren Rogan',
      author_email='darren@proroute.co',
      url='https://www.proroute.co',
      py_modules=['proroute_utilities','proroute_rest'],
     )