"""A collection of utility functions for ProRoute lambdas"""

#import system libraries in Lambda
import sys, os
import logging
import time
import uuid

#import 3rd party, libraries
import jsonpickle

#import classes
from proroute_utilities import ProRouteUtilities

#import AWS libs
import boto3
dynamodb = boto3.resource('dynamodb')


class ProRouteRestUtilities(object):

    """An :class:`ProRouteUtilities <proroute_utilities.proroute_utilities.ProRouteUtilities>` object."""
    def required_data_item_attrs():
        return ['name', 'version','query_type','query_string']

    def whitelist_data_fields():
        return ['name', 'version','query_type','query_string']

    def default_data_fields(helper):
        return {
            'version': 0
        }

    @classmethod
    def create_logic(cls, event, context, helper, logger, required_data_item_attrs, whitelist_data_fields, default_data_fields, tableName=None ):
        try:
            # Load data (Exceptions should bubble up to API Gateway)
            input_data = helper.load_input_data(event)
            logger.debug('loaded input_data {0}'.format(jsonpickle.encode(input_data)))

            # Validate required data is supplied
            helper.validate_input_data(input_data, required_data_item_attrs())
            logger.debug('valided input_data had required fields: {0}'.format(",".join(required_data_item_attrs())))

            # create the json object (whitelisted params only)
            item = helper.populate_new_item_from_data(input_data, whitelist_data_fields())
            logger.debug('populated item for creation {0}'.format(jsonpickle.encode(item)))

            # update items default(default_data_fields params only)
            item = helper.default_values(item,default_data_fields(helper))

            # get handle to the table
            if tableName:
                db_table = helper.get_dynamo_alt_env_db_table(tableName)
            else:
                db_table = helper.get_dynamo_db_table()
            logger.debug('Using table {0}'.format(db_table.name))

            # write the item to the database
            sanitized_item= helper.dynamodb_sanitize(item)
            response = db_table.put_item(Item=sanitized_item)
            logger.debug('Inserted item with response {0}'.format(jsonpickle.encode(response)))

            if response and response["ResponseMetadata"]["HTTPStatusCode"] == 200:
                return sanitized_item
            else:
                raise Exception('Failed to create, response: {0}'.format(jsonpickle.encode(response)))

        except Exception as e:
            logger.error('Exception {0}'.format(jsonpickle.encode(e)))
            raise e

    @classmethod
    def update_logic(cls, event, context, helper, logger,required_data_item_attrs, whitelist_data_fields, tableName=None ):
        try:
            # Load data (Exceptions should bubble up to API Gateway)
            input_data = helper.load_input_data(event)
            logger.debug('loaded input_data {0}'.format(jsonpickle.encode(input_data)))

            # Validate required data is supplied
            helper.validate_input_data(input_data, required_data_item_attrs())
            logger.debug('valided input_data had required fields: {0}'.format(",".join(required_data_item_attrs())))

            # Load data (Exceptions should bubble up to API Gateway)
            update_hash_key = event['path']['id']
            #update_range_key = event['path']['id']
            logger.debug('loaded get_hash_key {0}'.format(jsonpickle.encode(update_hash_key)))
        
            #get handle to the table
                    # get handle to the table
            if tableName:
                db_table = helper.get_dynamo_alt_env_db_table(tableName)
            else:
                db_table = helper.get_dynamo_db_table()
            logger.debug('Using table {0}'.format(db_table.name))

            # get the item from the database
            _update_hash_key = helper.dynamodb_sanitize(update_hash_key)
            result = db_table.get_item(
                Key={
                    'id': _update_hash_key
                }
            )
            logger.debug('get item with result {0}'.format(jsonpickle.encode(result)))


            if 'Item' not in result:
                logger.error('Update for record that does not exist with event: {0}'.format(jsonpickle.encode(event)))
                raise Exception('[404] Record not found')

            #grab the item from the table
            existing_item = result['Item']

            # create the json object (whitelisted params only)
            item = helper.populate_existing_item_from_data(input_data, existing_item, whitelist_data_fields())
            logger.debug('populated item for creation {0}'.format(jsonpickle.encode(item)))

            # write the item to the database
            sanitized_item= helper.dynamodb_sanitize(item)
            response = db_table.put_item(Item=sanitized_item)
            logger.debug('Inserted item with response {0}'.format(jsonpickle.encode(response)))

            if response and response["ResponseMetadata"]["HTTPStatusCode"] == 200:
                return sanitized_item
            else:
                raise Exception('Failed to update, response: {0}'.format(jsonpickle.encode(response)))

        except Exception as e:
            logger.error('Exception {0}'.format(jsonpickle.encode(e)))
            raise e

    @classmethod
    def get_logic(cls, event, context, helper, logger, tableName=None ):
        try:
            logger.debug('loaded get_logic {0}'.format(jsonpickle.encode(event)))

            # Load data (Exceptions should bubble up to API Gateway)
            get_hash_key = event['path']['id']
            logger.debug('loaded get_hash_key {0}'.format(jsonpickle.encode(get_hash_key)))

            # get handle to the table
            if tableName:
                db_table = helper.get_dynamo_alt_env_db_table(tableName)
            else:
                db_table = helper.get_dynamo_db_table()
            logger.debug('Using table {0}'.format(db_table.name))

            # get the item from the database
            _get_hash_key = helper.dynamodb_sanitize(get_hash_key)
            result = db_table.get_item(
                Key={
                    'id': _get_hash_key
                }
            )
            logger.debug('get item with result {0}'.format(jsonpickle.encode(result)))


        except ClientError as e:
            logger.error('ClientError {0}'.format(jsonpickle.encode(e)))
            raise e
        except Exception as e:
            logger.error('Exception {0}'.format(jsonpickle.encode(e)))
            raise e

        # create a response
        if 'Item' in result:
            try:
                response = result['Item']
                return response
            except Exception as e:
                logger.error('Exception {0}'.format(jsonpickle.encode(e)))
                raise e
        else:
            raise Exception('[404] Record not found')

    @classmethod
    def list_logic(cls, event, context, helper, logger, scan_filters_whitelist, tableName=None ):
        try:
            #get query data
            query_data = helper.load_query_data(event)
            logger.debug('loaded query_data {0}'.format(jsonpickle.encode(query_data)))

            #define the possible scan filters
            # scan_filters_whitelist = {
            #     'user_id':'string',
            #     'object_type':'string',
            #     'object_id':'string',
            #     'action': 'string'
            # }

            # get handle to the table
            if tableName:
                db_table = helper.get_dynamo_alt_env_db_table(tableName)
            else:
                db_table = helper.get_dynamo_db_table()
            logger.debug('Using table {0}'.format(db_table.name))

            # default logic for scan filter equal value
            op1 = helper.populate_scan_filter(query_data,scan_filters_whitelist)

            logger.debug('Using scan params {0}'.format(jsonpickle.encode(op1)))
            # operation_parameters = {
            #   'FilterExpression': 'bar > :x AND bar < :y',
            #   'ExpressionAttributeValues': {
            #     ':x': {'S': '2017-01-31T01:35'},
            #     ':y': {'S': '2017-01-31T02:08'},
            #   }
            # }

            # fetch items from the database
            response = db_table.scan(**op1)

            #paging later
            # paginator = dynamodb.get_paginator('scan')
            # page_iterator = paginator.paginate(**op1)
            # for page in page_iterator:
                
            return response
            
        except Exception as e:
            logger.error('Exception {0}'.format(jsonpickle.encode(e)))
            raise e

    @classmethod
    def delete_logic(cls, event,context,helper,logger,tableName=None):
        try:

            delete_hash_key = event['path']['id']
            logger.debug('loaded delete_hash_key {0}'.format(jsonpickle.encode(delete_hash_key)))

            #get handle to the table
            if tableName:
                db_table = helper.get_dynamo_alt_env_db_table(tableName)
            else:
                db_table = helper.get_dynamo_db_table()
            logger.debug('Using table {0}'.format(db_table.name))

            # delete the item from the database
            _get_hash_key = helper.dynamodb_sanitize(delete_hash_key)
            result = db_table.delete_item(
                Key={
                    'id': _get_hash_key
                }
            )
            logger.debug('Deleted item with result {0}'.format(jsonpickle.encode(result)))

            return result

        except Exception as e:
            logger.error('Exception {0}'.format(jsonpickle.encode(e)))
            raise e

    @classmethod
    def send_to_sns_dispatcher(item, env_sns_dispatch_arn, helper):
        try:
            sns_client = helper.get_boto3_client('sns')
            sns_dispatch_arn = helper.get_environ_prop(env_dispatch_arn)

            response = sns_client.publish(
                TargetArn=sns_dispatch_arn,
                Message=jsonpickle.encode({'default': item}),
                MessageStructure='json'
            )

            logger.info('send_to_dispatcher response {0}'.format(jsonpickle.encode(response)))
            return response
        except Exception as e:
            logger.error('Exception {0}'.format(jsonpickle.encode(e)))
            raise e